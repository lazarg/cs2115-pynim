import pygame
from definitions import *
from game import Game


class OverGame(Game):
    board = []
    options = [
        ("BACK", "Play again"),
        ("MAIN", "Go to main menu")
    ]
    previous = "MAIN"
    winner = 0
    selected = 0
    ended = False
    setup = False

    def __init__(self, previous, winner):
        self.previous = previous
        self.winner = winner

    def finished(self):
        return self.ended

    def get_next_game(self):
        if (self.options[self.selected][0] == "BACK"):
            return [self.previous]
        return [self.options[self.selected][0]]

    def get_winner(self):
        return 0

    def get_title(self):
        return "GAME OVER"

    def get_telemetry(self):
        res = [
            "Option: {}".format(self.selected)
        ]
        return res

    def think(self, events):
        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    self.selected = (self.selected - 1 + len(self.options)) % len(self.options)
                elif event.key == pygame.K_DOWN:
                    self.selected = (self.selected + 1) % len(self.options)
                elif event.key == pygame.K_RETURN:
                    self.ended = True

    def render(self, x, y):
        pygame.draw.rect(SCREEN, C_FORE(), pygame.Rect(x - 2, y - 2, 564, 564), 1)
        pygame.draw.rect(SCREEN, C_BACK(), pygame.Rect(x - 1, y - 1, 562, 562), 0)

        SCREEN.blit(F_36B.render("GAME OVER", True, C_FORE()), [x + 20, y + 20])

        render_menu(x + 20, y + 60, self.options, self.selected)
            
        
        SCREEN.blit(F_12B.render("WINNER", True, C_FORE()), [x + 20, y + 120 + len(self.options) * 40])
        if (self.winner == 1):
            SCREEN.blit(F_24B.render("Red", True, C_RED), [x + 20, y + 135 + len(self.options) * 40])
        elif (self.winner == 2):
            SCREEN.blit(F_24B.render("Blue", True, C_BLUE), [x + 20, y + 135 + len(self.options) * 40])
        else:
            SCREEN.blit(F_24B.render("No winner", True, C_FORE()), [x + 20, y + 135 + len(self.options) * 40])
