import pygame
from definitions import *
from game import *
import random


class GreedyNimGame(Game):
    MAX_WIDTH = 10
    MAX_HEIGHT = 10
    board = []
    selected = 0
    count = 0
    player = 1
    ended = False
    options = [
        ["Stacks:", [2,3,4,5,6,7,8,9,10], 0],
        ["First:", ["Red", "Blue"], 0],
        ["Blue:", ["Human", "AI"], 0]
    ]

    def __init__(self, board=[]):
        self.set_board(board)

    def finished(self):
        if self.ended:
            return True
        if self.setup:
            return False
        for c in self.board:
            if (c > 0):
                return False
        return True

    def get_next_game(self):
        return ["OVER", "GNIM"]

    def get_winner(self):
        if self.finished():
            return 3-self.player
        return 0

    def set_board(self, board):
        if (len(board) > self.MAX_WIDTH):
            board = board[0:self.MAX_WIDTH]
        for ind, col in enumerate(board):
            if (col > self.MAX_HEIGHT):
                board[ind] = self.MAX_HEIGHT
        self.board = board
        self.selected = 0
        self.count = 0
        self.player = self.options[1][2] + 1
        self.ended = False

    def generate_board(self):
        board = []
        for i in range(self.options[0][1][self.options[0][2]]):
            board.append(random.randint(1, self.MAX_HEIGHT+1))
        self.set_board(board)

    def get_title(self):
        return "GREEDY NIM"

    def get_telemetry(self):
        res = [
            "Board: "+(", ".join([str(elem) for elem in self.board])),
            "Selected: col {} cnt {}".format(self.selected, self.count),
            "Player: {}".format(self.player)
        ]
        return res

    def think(self, events):
        if self.setup:
            for event in events:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        self.selected = (self.selected + len(self.options) - 1) % len(self.options)
                    if event.key == pygame.K_DOWN:
                        self.selected = (self.selected + 1) % len(self.options)
                    if event.key == pygame.K_RIGHT:
                        self.options[self.selected][2] = (self.options[self.selected][2] + 1) % len(self.options[self.selected][1])
                    if event.key == pygame.K_LEFT:
                        self.options[self.selected][2] = (self.options[self.selected][2] + len(self.options[self.selected][1]) - 1) % len(self.options[self.selected][1])
                    if event.key == pygame.K_RETURN:
                        self.generate_board()
                        self.setup = False
        elif self.options[2][2] == 1 and self.player == 2:
            maximum = self.board[0]
            idx = 0
            cnt = 0
            second = 0
            sct = 0
            for i, c in enumerate(self.board):
                if (c > maximum):
                    second = maximum
                    sct = cnt
                    idx = i
                    cnt = 1
                    maximum = c
                elif (c == maximum):
                    cnt += 1
                elif (c > second):
                    second = c
                    sct = 1
                elif (c == second):
                    sct += 1
            if second == 0 and maximum > 1:
                second = 1
            if (sct % 2 == cnt % 2) and (maximum > 1):
                self.board[idx] = second
            else:
                self.board[idx] = 0
            self.player = 3 - self.player
        else:
            maximum = self.board[0]
            for c in self.board:
                if (c > maximum):
                    maximum = c
            while (self.board[self.selected] != maximum):
                self.selected = (self.selected + 1) % len(self.board)
            for event in events:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        self.selected = (self.selected - 1 + len(self.board)) % len(self.board)
                        while (self.board[self.selected] != maximum):
                            self.selected = (self.selected - 1 + len(self.board)) % len(self.board)
                        self.count = 0
                    elif event.key == pygame.K_RIGHT:
                        self.selected = (self.selected + 1) % len(self.board)
                        while (self.board[self.selected] != maximum):
                            self.selected = (self.selected + 1) % len(self.board)
                        self.count = 0
                    elif event.key == pygame.K_UP:
                        self.count -= 1
                        if (self.count < 0):
                            self.count = 0
                    elif event.key == pygame.K_DOWN:
                        self.count += 1
                        if (self.count > self.board[self.selected]):
                            self.count = self.board[self.selected]
                    elif event.key == pygame.K_r:
                        self.player = 0
                        self.ended = True
                    elif event.key == pygame.K_RETURN:
                        if (self.count > 0):
                            self.board[self.selected] -= self.count
                            self.count = 0
                            self.player = 3 - self.player

    def render(self, x, y):
        pygame.draw.rect(SCREEN, C_FORE(), pygame.Rect(x - 2, y - 2, 564, 564), 1)
        pygame.draw.rect(SCREEN, C_BACK(), pygame.Rect(x - 1, y - 1, 562, 562), 0)
        if self.setup:
            SCREEN.blit(F_36B.render("GREEDY NIM - SETUP", True, C_FORE()), [x + 20, y + 20])
            render_setup(x + 20, y + 60, self.options, self.selected)
        else:
            SCREEN.blit(F_36B.render("GREEDY NIM", True, C_FORE()), [x + 20, y + 20])
            SCREEN.blit(F_12B.render("NOW PLAYING", True, C_FORE()), [x + 20, y + 75])
            if (self.player == 1):
                SCREEN.blit(F_24B.render("Red", True, C_RED), [x + 20, y + 90])
            else:
                SCREEN.blit(F_24B.render("Blue", True, C_BLUE), [x + 20, y + 90])

            render_board(x + 20, y + 290, 520, self.board, self.selected, self.count)
