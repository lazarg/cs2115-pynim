import pygame
import os
import sys


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


pygame.init()

# SETTING COLORS
C_BLACK = (0, 0, 0)
C_BLUE = (0, 162, 232)
C_DARK = (63, 63, 63)
C_GREEN = (34, 177, 76)
C_LIGHT = (153, 217, 234)
C_PINK = (255, 100, 150)
C_PURPLE = (163, 73, 164)
C_RED = (237, 28, 36)
C_YELLOW = (255, 201, 14)
C_WHITE = (255, 255, 255)

IS_DARK = True

def set_dark_mode(is_dark):
    global IS_DARK
    IS_DARK = is_dark

def C_BACK():
    global IS_DARK
    if IS_DARK:
        return (25, 25, 25)
    else:
        return (230, 230, 230)
def C_FORE():
    global IS_DARK
    if IS_DARK:
        return (230, 230, 230)
    else:
        return (25, 25, 25)
def C_SELB():
    global IS_DARK
    if IS_DARK:
        return (0, 162, 232)
    else:
        return (255, 201, 14)
def C_SELF():
    global IS_DARK
    if IS_DARK:
        return (255, 201, 14)
    else:
        return (0, 162, 232)


# SETTING FONTS
F_48 = pygame.font.Font(resource_path("res/fontr.ttf"), 48)
F_36 = pygame.font.Font(resource_path("res/fontr.ttf"), 36)
F_24 = pygame.font.Font(resource_path("res/fontr.ttf"), 24)
F_18 = pygame.font.Font(resource_path("res/fontr.ttf"), 18)
F_12 = pygame.font.Font(resource_path("res/fontr.ttf"), 12)
F_48B = pygame.font.Font(resource_path("res/fontb.ttf"), 48)
F_36B = pygame.font.Font(resource_path("res/fontb.ttf"), 36)
F_24B = pygame.font.Font(resource_path("res/fontb.ttf"), 24)
F_18B = pygame.font.Font(resource_path("res/fontb.ttf"), 18)
F_12B = pygame.font.Font(resource_path("res/fontb.ttf"), 12)

# LOADING IMAGES
B_PLAIN = pygame.image.load(resource_path("res/b-plain.png"))
B_SELA = pygame.image.load(resource_path("res/b-selected-a.png"))
B_SELB = pygame.image.load(resource_path("res/b-selected-b.png"))
B_DEL = pygame.image.load(resource_path("res/b-deleted.png"))
B_ARRDN = pygame.image.load(resource_path("res/b-arrow-down.png"))
B_ARRUP = pygame.image.load(resource_path("res/b-arrow-up.png"))

SIZE = WIDTH, HEIGHT = 800, 600

SCREEN = pygame.display.set_mode(SIZE)
pygame.display.set_caption("PyNim")
pygame.display.set_icon(pygame.image.load(resource_path("res/icon.png")))
SCREEN.fill(C_BACK())

def render_square(x, y, type):
    if (type == "SELECTED"):
        pygame.draw.rect(SCREEN, C_SELF(), pygame.Rect(x + 1, y + 1, 18, 18), 0)
    elif (type == "DELETED"):
        sign = F_18B.render("X", True, C_FORE())
        SCREEN.blit(sign, [x + 10 - sign.get_width() // 2, y + 10 - sign.get_height() // 2])
    elif (type == "ARROW_DOWN"):
        sign = F_18B.render("V", True, C_SELF())
        SCREEN.blit(sign, [x + 10 - sign.get_width() // 2, y + 10 - sign.get_height() // 2])
    elif (type == "ARROW_UP"):
        sign = F_18B.render("Λ", True, C_SELF())
        SCREEN.blit(sign, [x + 10 - sign.get_width() // 2, y + 10 - sign.get_height() // 2])
    else:
        pygame.draw.rect(SCREEN, C_FORE(), pygame.Rect(x + 1, y + 1, 18, 18), 0)
    pygame.draw.rect(SCREEN, C_BACK(), pygame.Rect(x, y, 20, 20), 1)

def render_board(x, y, width, board, selected, count):
    l = x + width // 2 - len(board) * 15 + 5
    for idx, h in enumerate(board):
        if (idx == selected):
            for t in range(y + 225 - 20 * h, y + 225 - 20 * (h - count), 20):
                render_square(l, t, "SELECTED")
            for t in range(y + 225 - 20 * (h - count), y + 225, 20):
                render_square(l, t, "PLAIN")
            if h == 0:
                h = 1
                render_square(l, y + 205, "DELETED")
            render_square(l, y, "ARROW_DOWN")
            render_square(l, y + 230, "ARROW_UP")
        else:
            for t in range(y + 225 - 20 * h, y + 225, 20):
                render_square(l, t, "PLAIN")
            if h == 0:
                h = 1
                render_square(l, y + 205, "DELETED")
        l += 30

def render_menu(x, y, options, selected):
    SCREEN.blit(F_12.render("Switch options with UP and DOWN keys, choose with ENTER", True, C_FORE()), [x, y])
    for idx,opt in enumerate(options):
        if (idx == selected):
            pygame.draw.rect(SCREEN, C_FORE(), pygame.Rect(x - 10, y + 15 + idx * 40, 540, 40), 0)
            SCREEN.blit(F_24B.render(opt[1], True, C_BACK()), [x, y + 20 + idx * 40])
        else:
            SCREEN.blit(F_24B.render(opt[1], True, C_FORE()), [x, y + 20 + idx * 40])

def render_setup(x, y, options, selected):
    SCREEN.blit(F_12.render("Press UP and DOWN to select option, LEFT and RIGHT to change, ENTER to proceed", True, C_FORE()), [x, y])
    for idx, opt in enumerate(options):
        if (idx == selected):
            pygame.draw.rect(SCREEN, C_FORE(), pygame.Rect(x - 10, y + 15 + idx * 40, 540, 40), 0)
            SCREEN.blit(F_24B.render(opt[0], True, C_BACK()), [x, y + 20 + idx * 40])
            pos = 0
            for i, val in enumerate(opt[1]):
                txt = F_24.render(str(val), True, C_BACK())
                if i == opt[2]:
                    pygame.draw.rect(SCREEN, C_SELB(), pygame.Rect(x + 199 + pos, y + 17 + idx * 40, txt.get_width() + 12, 36), 3)
                SCREEN.blit(txt, [x + 200 + pos + 5, y + 20 + idx * 40])
                pos += txt.get_width() + 10
        else:
            pygame.draw.rect(SCREEN, C_BACK(), pygame.Rect(x - 10, y + 15 + idx * 40, 540, 40), 0)
            SCREEN.blit(F_24B.render(opt[0], True, C_FORE()), [x, y + 20 + idx * 40])
            pos = 0
            for i, val in enumerate(opt[1]):
                txt = F_24.render(str(val), True, C_FORE())
                if i == opt[2]:
                    pygame.draw.rect(SCREEN, C_SELF(), pygame.Rect(x + 199 + pos, y + 17 + idx * 40, txt.get_width() + 12, 36), 3)
                SCREEN.blit(txt, [x + 200 + pos + 5, y + 20 + idx * 40])
                pos += txt.get_width() + 10
