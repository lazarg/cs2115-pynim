import pygame
from pygame import register_quit
from definitions import *
from game import *
import random


class SuperNimGame(Game):
    MAX_WIDTH = 10
    MAX_HEIGHT = 10
    regbd = []
    misbd = []
    regwn = 0
    miswn = 0
    selected = 0
    count = 0
    player = 1
    ended = False
    boosted = False
    boosts = 1
    options = [
        ["Reg. stacks:", [2,3,4,5,6,7,8], 0],
        ["Mis. stacks:", [2,3,4,5,6,7,8], 0],
        ["First:", ["Red", "Blue"], 0],
        ["Blue:", ["Human", "AI"], 0],
        ["Boost:", ["Red", "Blue"], 0]
    ]

    def __init__(self, regbd=[], misbd=[]):
        self.set_board(regbd, misbd)

    def finished(self):
        if self.ended:
            return True
        if self.setup:
            return False
        if self.regwn * self.miswn > 0:
            return True
        for c in (self.regbd + self.misbd):
            if (c > 0):
                return False
        return True

    def get_next_game(self):
        return ["OVER", "SNIM"]

    def get_winner(self):
        if self.regwn == self.miswn:
            return self.regwn
        return 0

    def set_board(self, regbd, misbd):
        if (len(regbd) > self.MAX_WIDTH):
            regbd = regbd[0:self.MAX_WIDTH]
        for ind, col in enumerate(regbd):
            if (col > self.MAX_HEIGHT):
                regbd[ind] = self.MAX_HEIGHT
        if (len(misbd) > self.MAX_WIDTH):
            misbd = misbd[0:self.MAX_WIDTH]
        for ind, col in enumerate(misbd):
            if (col > self.MAX_HEIGHT):
                misbd[ind] = self.MAX_HEIGHT
        self.regbd = regbd
        self.misbd = misbd
        self.selected = 0
        self.count = 0
        self.player = self.options[2][2] + 1
        self.ended = False

    def generate_board(self):
        regbd = []
        misbd = []
        for i in range(self.options[0][1][self.options[0][2]]):
            regbd.append(random.randint(1, self.MAX_HEIGHT+1))
        for i in range(self.options[1][1][self.options[1][2]]):
            misbd.append(random.randint(1, self.MAX_HEIGHT+1))
        self.set_board(regbd, misbd)

    def get_title(self):
        return "SUPERNIM"

    def get_telemetry(self):
        res = [
            "Reg. board: "+(", ".join([str(elem) for elem in self.regbd])),
            "Mis. board: "+(", ".join([str(elem) for elem in self.misbd])),
            "Reg. win: {}".format(self.regwn),
            "Mis. win: {}".format(self.miswn),
            "Selected: col {} cnt {}".format(self.selected, self.count),
            "Player: {}".format(self.player)
        ]
        return res

    def get_count(self, idx):
        if (idx < len(self.regbd)):
            return self.regbd[idx]
        elif (idx < len(self.regbd) + len(self.misbd)):
            return self.misbd[idx - len(self.regbd)]
        else:
            return self.boosts

    def set_count(self, idx, val):
        if (idx < len(self.regbd)):
            self.regbd[idx] = val
        else:
            self.misbd[idx - len(self.regbd)] = val

    def check(self):
        t = True
        for c in self.regbd:
            if (c > 0):
                t = False
        if t and self.regwn == 0:
            self.regwn = self.player
        t = True
        for c in self.misbd:
            if (c > 0):
                t = False
        if t and self.miswn == 0:
            self.miswn = 3 - self.player

    def winpos_reg(self):
        nimsum = 0
        for c in self.regbd:
            nimsum = nimsum ^ c
        return nimsum == 0

    def winpos_mis(self):
        nimsum = 0
        multips = 0
        singles = 0
        for c in self.misbd:
            nimsum = nimsum ^ c
            if c > 1:
                multips += 1
            elif c == 1:
                singles += 1
        if (multips > 1):
            return nimsum == 0
        elif (multips == 1):
            return True
        return (singles % 2) == 1

    def play_reg(self):
        nimsum = 0
        for c in self.regbd:
            nimsum = nimsum ^ c
        i = 0
        while i < len(self.regbd) and (self.regbd[i] ^ nimsum) >= self.regbd[i]:
            i += 1
        if i < len(self.regbd):
            self.regbd[i] = self.regbd[i] ^ nimsum
        else:
            i = random.randint(0, len(self.regbd) - 1)
            while (not self.finished()) and self.regbd[i] == 0:
                i = (i + 1) % len(self.regbd)
            self.regbd[i] = random.randint(0, self.regbd[i] - 1)
        self.check()
        self.player = 3 - self.player

    def play_mis(self):
        nimsum = 0
        multips = 0
        singles = 0
        newval = 1
        for c in self.misbd:
            nimsum = nimsum ^ c
            if c > 1:
                multips += 1
            elif c == 1:
                singles += 1
        idx = 0
        if (multips > 1):
            while idx < len(self.misbd) and (self.misbd[idx] ^ nimsum) >= self.misbd[idx]:
                idx += 1
            if (idx < len(self.misbd)):
                newval = self.misbd[idx] ^ nimsum
        elif (multips == 1):
            newval = (singles + 1) % 2
            while idx < len(self.misbd) and self.misbd[idx] <= 1:
                idx += 1
        else:
            newval = 0
            while idx < len(self.misbd) and self.misbd[idx] == 0:
                idx += 1
        if idx < len(self.misbd):
            self.misbd[idx] = newval
        else:
            idx = random.randint(0, len(self.misbd) - 1)
            while (not self.finished()) and self.misbd[idx] == 0:
                idx = (idx + 1) % len(self.misbd)
            self.misbd[idx] = self.misbd[idx] // 2
        self.check()
        self.player = 3 - self.player

    def think(self, events):
        if self.setup:
            for event in events:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        self.selected = (self.selected + len(self.options) - 1) % len(self.options)
                    if event.key == pygame.K_DOWN:
                        self.selected = (self.selected + 1) % len(self.options)
                    if event.key == pygame.K_RIGHT:
                        self.options[self.selected][2] = (self.options[self.selected][2] + 1) % len(self.options[self.selected][1])
                    if event.key == pygame.K_LEFT:
                        self.options[self.selected][2] = (self.options[self.selected][2] + len(self.options[self.selected][1]) - 1) % len(self.options[self.selected][1])
                    if event.key == pygame.K_RETURN:
                        self.generate_board()
                        self.setup = False
        elif self.options[3][2] == 1 and self.player == 2:
            evreg = self.winpos_reg()
            evmis = self.winpos_mis()

            if (not evreg) and (not evmis) and (self.boosts > 0) and (self.options[4][2] == (self.player - 1)) and (self.regwn == 0) and (self.miswn == 0):
                self.boosts -= 1
                self.play_reg()
                self.player = 3 - self.player
                self.play_mis()
            elif not evreg and self.regwn == 0:
                self.play_reg()
            elif not evmis and self.miswn == 0:
                self.play_mis()
            elif self.regwn == 0:
                self.play_reg()
            else:
                self.play_mis()
        else:
            maxsel = len(self.regbd) + len(self.misbd)
            if (self.boosts > 0) and (self.options[4][2] == (self.player - 1)):
                maxsel += 1
            while self.get_count(self.selected) == 0:
                self.selected = (self.selected + 1) % maxsel
            for event in events:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        self.selected = (self.selected - 1 + maxsel) % maxsel
                        while self.get_count(self.selected) == 0:
                            self.selected = (self.selected - 1 + maxsel) % maxsel
                        self.count = 0
                    elif event.key == pygame.K_RIGHT:
                        self.selected = (self.selected + 1) % maxsel
                        while self.get_count(self.selected) == 0:
                            self.selected = (self.selected + 1) % maxsel
                        self.count = 0
                    elif event.key == pygame.K_UP:
                        self.count -= 1
                        if (self.count < 0):
                            self.count = 0
                    elif event.key == pygame.K_DOWN:
                        self.count += 1
                        if self.count > self.get_count(self.selected):
                            self.count = self.get_count(self.selected)
                    elif event.key == pygame.K_r:
                        self.player = 0
                        self.ended = True
                    elif event.key == pygame.K_RETURN:
                        if (self.selected != len(self.regbd) + len(self.misbd)):
                            if (self.count > 0):
                                self.set_count(self.selected, self.get_count(self.selected) - self.count)
                                self.count = 0
                                self.check()
                                if not self.boosted:
                                    self.player = 3 - self.player
                                else:
                                    self.boosted = False
                        else:
                            if (self.boosts > 0) and (self.options[4][2] == (self.player - 1)):
                                self.boosts -= 1
                                self.boosted = True



    def render(self, x, y):
        pygame.draw.rect(SCREEN, C_FORE(), pygame.Rect(x - 2, y - 2, 564, 564), 1)
        pygame.draw.rect(SCREEN, C_BACK(), pygame.Rect(x - 1, y - 1, 562, 562), 0)
        if self.setup:
            SCREEN.blit(F_36B.render("SUPERNIM - SETUP", True, C_FORE()), [x + 20, y + 20])
            render_setup(x + 20, y + 60, self.options, self.selected)
        else:
            SCREEN.blit(F_36B.render("SUPERNIM", True, C_FORE()), [x + 20, y + 20])
            SCREEN.blit(F_12B.render("NOW PLAYING", True, C_FORE()), [x + 20, y + 75])
            if (self.player == 1):
                SCREEN.blit(F_24B.render("Red", True, C_RED), [x + 20, y + 90])
            else:
                SCREEN.blit(F_24B.render("Blue", True, C_BLUE), [x + 20, y + 90])
            if (self.boosted):
                SCREEN.blit(F_12B.render("BOOSTED - CAN PLAY TWICE", True, C_FORE()), [x + 20, y + 120])

            render_board(x + 20, y + 290, 250, self.regbd, self.selected, self.count)
            render_board(x + 290, y + 290, 250, self.misbd, self.selected - len(self.regbd), self.count)

            boost_color = C_FORE()
            phrase = "BOOST!"
            if (self.selected == len(self.regbd) + len(self.misbd)):
                boost_color = C_SELF()
                phrase = "> BOOST! <"
            boost_text = F_24B.render(phrase, True, boost_color)
            if (self.boosts > 0) and (self.options[4][2] == (self.player - 1)):
                SCREEN.blit(boost_text, [x + 280 - boost_text.get_width() // 2, y + 280 - boost_text.get_height()])
