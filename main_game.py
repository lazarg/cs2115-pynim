import pygame
from definitions import *
from game import Game


class MainGame(Game):
    board = []
    options = [
        ("RNIM", "Regular Nim"),
        ("MNIM", "Misère Nim"),
        ("GNIM", "Greedy Nim"),
        ("SNIM", "SuperNim"),
        ("QUIT", "Quit game")
    ]
    selected = 0
    ended = False
    setup = False

    def __init__(self):
        pass

    def finished(self):
        return self.ended

    def get_next_game(self):
        return [self.options[self.selected][0]]

    def get_winner(self):
        return 0

    def get_title(self):
        return "MAIN MENU"

    def get_telemetry(self):
        res = [
            "Option: {}".format(self.selected)
        ]
        return res

    def think(self, events):
        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    self.selected = (self.selected - 1 + len(self.options)) % len(self.options)
                elif event.key == pygame.K_DOWN:
                    self.selected = (self.selected + 1) % len(self.options)
                elif event.key == pygame.K_RETURN:
                    self.ended = True

    def render(self, x, y):
        pygame.draw.rect(SCREEN, C_FORE(), pygame.Rect(x - 2, y - 2, 564, 564), 1)
        pygame.draw.rect(SCREEN, C_BACK(), pygame.Rect(x - 1, y - 1, 562, 562), 0)

        SCREEN.blit(F_36B.render("MAIN MENU", True, C_FORE()), [x + 20, y + 20])

        render_menu(x + 20, y + 60, self.options, self.selected)
