import pygame
from definitions import *


class Game:
    board = []
    setup = True

    def __init__(self):
        pass

    def finished(self):
        return True

    def get_next_game(self):
        return ["MAIN"]

    def get_winner(self):
        return 0

    def get_title(self):
        return "PROTO-GAME"

    def get_telemetry(self):
        res = [
            "You should not see this",
            "(except while quitting)",
            "If you do, take a screenshot",
            "and contact the devs at:",
            "lazargalic.gale@gmail.com"
        ]
        return res

    def think(self, events):
        pass

    def render(self, x, y):
        pygame.draw.rect(SCREEN, C_FORE(), pygame.Rect(x - 2, y - 2, 564, 564), 1)
        pygame.draw.rect(SCREEN, C_BACK(), pygame.Rect(x - 1, y - 1, 562, 562), 0)

        SCREEN.blit(F_12B.render("GAME ENDED", True, C_FORE()), [x + 20, y + 20])
        SCREEN.blit(F_24B.render("BYE BYE!", True, C_FORE()), [x + 20, y + 35])
