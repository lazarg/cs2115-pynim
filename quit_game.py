import pygame
from definitions import *
from game import Game


class QuitGame(Game):
    def __init__(self):
        pass

    def get_title(self):
        return "QUIT"
