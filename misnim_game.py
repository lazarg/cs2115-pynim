import pygame
from definitions import *
from game import *
import random


class MisNimGame(Game):
    MAX_WIDTH = 10
    MAX_HEIGHT = 10
    board = []
    selected = 0
    count = 0
    player = 1
    ended = False
    options = [
        ["Stacks:", [2,3,4,5,6,7,8,9,10], 0],
        ["First:", ["Red", "Blue"], 0],
        ["Blue:", ["Human", "AI"], 0]
    ]

    def __init__(self, board=[]):
        self.set_board(board)

    def finished(self):
        if self.ended:
            return True
        if self.setup:
            return False
        for c in self.board:
            if (c > 0):
                return False
        return True

    def get_next_game(self):
        return ["OVER", "MNIM"]

    def get_winner(self):
        if self.finished():
            return self.player
        return 0

    def set_board(self, board):
        if (len(board) > self.MAX_WIDTH):
            board = board[0:self.MAX_WIDTH]
        for ind, col in enumerate(board):
            if (col > self.MAX_HEIGHT):
                board[ind] = self.MAX_HEIGHT
        self.board = board
        self.selected = 0
        self.count = 0
        self.player = self.options[1][2] + 1
        self.ended = False

    def generate_board(self):
        board = []
        for i in range(self.options[0][1][self.options[0][2]]):
            board.append(random.randint(1, self.MAX_HEIGHT+1))
        self.set_board(board)

    def get_title(self):
        return "MISÈRE NIM"

    def get_telemetry(self):
        res = [
            "Board: "+(", ".join([str(elem) for elem in self.board])),
            "Selected: col {} cnt {}".format(self.selected, self.count),
            "Player: {}".format(self.player)
        ]
        return res

    def think(self, events):
        if self.setup:
            for event in events:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        self.selected = (self.selected + len(self.options) - 1) % len(self.options)
                    if event.key == pygame.K_DOWN:
                        self.selected = (self.selected + 1) % len(self.options)
                    if event.key == pygame.K_RIGHT:
                        self.options[self.selected][2] = (self.options[self.selected][2] + 1) % len(self.options[self.selected][1])
                    if event.key == pygame.K_LEFT:
                        self.options[self.selected][2] = (self.options[self.selected][2] + len(self.options[self.selected][1]) - 1) % len(self.options[self.selected][1])
                    if event.key == pygame.K_RETURN:
                        self.generate_board()
                        self.setup = False
        elif self.options[2][2] == 1 and self.player == 2:
            nimsum = 0
            multips = 0
            singles = 0
            newval = 1
            for c in self.board:
                nimsum = nimsum ^ c
                if c > 1:
                    multips += 1
                elif c == 1:
                    singles += 1
            idx = 0
            if (multips > 1):
                while idx < len(self.board) and (self.board[idx] ^ nimsum) >= self.board[idx]:
                    idx += 1
                if (idx < len(self.board)):
                    newval = self.board[idx] ^ nimsum
            elif (multips == 1):
                newval = (singles + 1) % 2
                while idx < len(self.board) and self.board[idx] <= 1:
                    idx += 1
            else:
                newval = 0
                while idx < len(self.board) and self.board[idx] == 0:
                    idx += 1
            if idx < len(self.board):
                self.board[idx] = newval
            else:
                idx = random.randint(0, len(self.board) - 1)
                while (not self.finished()) and self.board[idx] == 0:
                    idx = (idx + 1) % len(self.board)
                self.board[idx] = self.board[idx] // 2
            self.player = 3 - self.player
        else:
            for event in events:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        self.selected = (self.selected - 1 + len(self.board)) % len(self.board)
                        while (self.board[self.selected] == 0):
                            self.selected = (self.selected - 1 + len(self.board)) % len(self.board)
                        self.count = 0
                    elif event.key == pygame.K_RIGHT:
                        self.selected = (self.selected + 1) % len(self.board)
                        while (self.board[self.selected] == 0):
                            self.selected = (self.selected + 1) % len(self.board)
                        self.count = 0
                    elif event.key == pygame.K_UP:
                        self.count -= 1
                        if (self.count < 0):
                            self.count = 0
                    elif event.key == pygame.K_DOWN:
                        self.count += 1
                        if (self.count > self.board[self.selected]):
                            self.count = self.board[self.selected]
                    elif event.key == pygame.K_r:
                        self.player = 0
                        self.ended = True
                    elif event.key == pygame.K_RETURN:
                        if (self.count > 0):
                            self.board[self.selected] -= self.count
                            self.count = 0
                            self.player = 3 - self.player

    def render(self, x, y):
        pygame.draw.rect(SCREEN, C_FORE(), pygame.Rect(x - 2, y - 2, 564, 564), 1)
        pygame.draw.rect(SCREEN, C_BACK(), pygame.Rect(x - 1, y - 1, 562, 562), 0)
        if self.setup:
            SCREEN.blit(F_36B.render("MISÈRE NIM - SETUP", True, C_FORE()), [x + 20, y + 20])
            render_setup(x + 20, y + 60, self.options, self.selected)
        else:
            SCREEN.blit(F_36B.render("MISÈRE NIM", True, C_FORE()), [x + 20, y + 20])
            SCREEN.blit(F_12B.render("NOW PLAYING", True, C_FORE()), [x + 20, y + 75])
            if (self.player == 1):
                SCREEN.blit(F_24B.render("Red", True, C_RED), [x + 20, y + 90])
            else:
                SCREEN.blit(F_24B.render("Blue", True, C_BLUE), [x + 20, y + 90])

            render_board(x + 20, y + 290, 520, self.board, self.selected, self.count)
