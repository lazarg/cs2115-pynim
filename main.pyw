# compile with: pyinstaller main.pyw --onefile --add-data "./res;./res" --icon "./res/icon.ico"

import pygame

from definitions import *
from main_game import MainGame
from quit_game import QuitGame
from regnim_game import RegNimGame
from misnim_game import MisNimGame
from greedynim_game import GreedyNimGame
from supernim_game import SuperNimGame
from over_game import OverGame


def get_telemetry(game):
    res = [
        "TELEMETRY",
        "======================",
        "Frame: {}".format(step),
        "Type: {}".format(game.get_title()),
        "Setup: {}".format(game.setup)
    ]
    if (winner > 0):
        res.append("Winner: Player {}".format(winner))
    else:
        res.append("Winner: No winner")
    res.extend(game.get_telemetry())
    res.reverse()
    return res


GAME = {
    "MAIN": MainGame,
    "QUIT": QuitGame,
    "RNIM": RegNimGame,
    "MNIM": MisNimGame,
    "GNIM": GreedyNimGame,
    "SNIM": SuperNimGame,
    "OVER": OverGame
}


step = 0

game = GAME["MAIN"]()
telemetry = False
playing = True
winner = 0

dark_mode = False
set_dark_mode(dark_mode)

def get_lines():
    return [
        (True, "CS2115 Homework 3"),
        (False, ""),
        (True, "11_Group 2"),
        (False, "KAEWNUKULTORN Nuttachon"),
        (False, "MOUSTAFA Omar"),
        (False, "KHANH Ly Dinh"),
        (False, "GALIĆ Lazar"),
        (False, "JELAČA Aleksa"),
        (False, ""),
        (False, "R - Restart game"),
        (False, "M - Toggle {} mode".format("light" if dark_mode else "dark")),
        (False, "T - {} telemetry".format("Hide" if telemetry else "Show")),
        (False, "ESC - Quit game")   
    ]

while playing:
    events = pygame.event.get()

    game.think(events)

    SCREEN.fill(C_BACK())

    SCREEN.blit(F_24B.render("PyNim", True, C_FORE()), [20, 20])
    for idx, val in enumerate(get_lines()):
        if (val[0]):
            text = F_12B.render(val[1], True, C_SELF())
        else:
            text = F_12.render(val[1], True, C_FORE())
        SCREEN.blit(text, [20, 50 + idx*15])

    # pygame.time.wait(20)

    if (game.finished()):
        winner = game.get_winner()
        next_game = game.get_next_game()
        if (len(next_game) > 1):
            game = GAME[next_game[0]](next_game[1], winner)
        else:
            game = GAME[next_game[0]]()

    if (game.get_title() == "QUIT"):
        playing = False

    if (telemetry):
        for idx, val in enumerate(get_telemetry(game)):
            SCREEN.blit(F_12.render(val, True, C_SELF()), [20, 568 - idx*14])

    for event in events:
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_t:
                telemetry = not telemetry
            elif event.key == pygame.K_ESCAPE:
                playing = False
            elif event.key == pygame.K_m:
                dark_mode = not dark_mode
                set_dark_mode(dark_mode)
                from definitions import *
        elif event.type == pygame.QUIT:
            playing = False

    game.render(220, 20)

    pygame.draw.rect(SCREEN, C_FORE(), pygame.Rect(226, 8, 154, 19), 1)
    pygame.draw.rect(SCREEN, C_BACK(), pygame.Rect(227, 9, 152, 17), 0)
    SCREEN.blit(F_12B.render(game.get_title(), True, C_FORE()), [230, 10])

    # sending display to buffer
    pygame.display.flip()
    step += 1
